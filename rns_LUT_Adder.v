`timescale 1ns / 1ps
module FIR(
	input clk,reset,enable_fir,
        input signed [15:0] inSample,
        output reg signed [31:0] outSample
        );
        parameter tapWidth = 4;
        reg [15:0] buffer [0:tapWidth-1];
        reg [32:0] sum [0:tapWidth];
        reg [15:0] temp;
        integer i;
	integer j;

	reg [7:0] add8 [0:7][0:7];
        reg [7:0] add7 [0:6][0:6];
        reg [7:0] add5 [0:4][0:4];
        reg [7:0] add3 [0:2][0:2];
        reg [7:0] mul8 [0:7][0:7];
        reg [7:0] mul7 [0:6][0:6];
        reg [7:0] mul5 [0:4][0:4];
        reg [7:0] mul3 [0:2][0:2];
        initial
        begin
            $readmemb("LUTmul8.txt", mul8);
	    $readmemb("LUTmul7.txt", mul7);
	    $readmemb("LUTmul5.txt", mul5);
	    $readmemb("LUTmul3.txt", mul3);
            $readmemb("LUT8.txt", add8);
	    $readmemb("LUT7.txt", add7);
	    $readmemb("LUT5.txt", add5);
	    $readmemb("LUT3.txt", add3);
            //$display (add8[7][6]);
            //#7542
            //#6431
            //$display ("%b", {add8[7][6],add7[5][4],add5[4][3],add3[2][1]});
	    $display ("%b", {add8[num1[11:8]][num2[11:8]],add7[num1[7:5]][num2[7:5]],add5[num1[4:2]][num2[4:2]],add3[num1[1:0]][num2[1:0]]});
        end
endmodule

module tst;
        reg clk;
        reg rst;
        reg en;
        reg [15:0] inputSample;
        wire [31:0] dataout;
        FIR uut (
                clk,
                rst,
                en,
                inputSample,
                dataout
        );
        initial begin
        $monitor ($time, " Read Data = %d" ,dataout);
        $dumpfile("wave.vcd"); 
        $dumpvars(0, tst);
        clk = 0;
        rst = 0;
        en = 0;
        inputSample = 0;
        #100;
        rst = 1;
        #100;
        rst = 0;
        inputSample = 8'd5;
        #100;
        $finish;
        end
        always begin #50 clk=~clk; end
endmodule