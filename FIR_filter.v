`timescale 1ns / 1ps
module FIR(
	input clk,reset,enable_fir,
        input signed [15:0] inSample,
        output reg signed [31:0] outSample
        );
        parameter tapWidth = 14;
        reg [15:0] weights [0:tapWidth-1];
        reg [15:0] buffer [0:tapWidth-1];
        reg [32:0] sum [0:tapWidth];
        reg [15:0] temp;
        integer i;
        initial
        begin
            $readmemb("weights.txt", weights);
            for (i = 0; i < tapWidth; i = i+1) 
                begin
                        buffer[i] = 0;
                end
        end

        always@ (posedge clk)
                begin
                        buffer[0] <= inSample;
                        for (i = tapWidth-1; i > 0; i = i-1) 
                                begin
                                        buffer[i] <= buffer[i-1];
                                end
                        outSample = 0;
                        for (i = 0; i < tapWidth; i = i+1) 
                        begin
                                sum[i] = buffer[i] * weights[i];
                                outSample = outSample + sum[i];
                        end 
                end
endmodule

module tst;
        reg clk;
        reg rst;
        reg en;
        reg [15:0] inputSample;
        wire [31:0] dataout;
        FIR uut (
                clk,
                rst,
                en,
                inputSample,
                dataout
        );
        initial begin
        $monitor ($time, " Read Data = %d" ,dataout);
        $dumpfile("wave.vcd"); 
        $dumpvars(0, tst);
        clk = 0;
        rst = 0;
        en = 0;
        inputSample = 0;
        #100;
        rst = 1;
        #100;
        rst = 0;
        inputSample = 8'd5;
        #100;
        inputSample = 8'd10;
        #100;
        inputSample = 8'd12;
        #100;
        en = 1'b1;
        inputSample = 8'd15;
        #100;
        inputSample = 8'd16;
        #100;
        inputSample = 8'd0;
        #1000;
        $finish;
        end
        always begin #50 clk=~clk; end
endmodule